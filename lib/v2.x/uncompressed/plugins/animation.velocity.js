/*!
 * ScrollMagic v2.0.8 (2020-08-14)
 * The javascript library for magical scroll interactions.
 * (c) 2020 Jan Paepke (@janpaepke)
 * Project Website: http://scrollmagic.io
 *
 * @version 2.0.8
 * @license Dual licensed under MIT license and GPL.
 * @author Jan Paepke - e-mail@janpaepke.de
 *
 * @file ScrollMagic ScrollMagic Animation Plugin.
 *
 * requires: scrollmagic ~1.2
 * Powered by ScrollMagicJS: http://ScrollMagicJS.org
 * ScrollMagic is published under MIT license.
 */
/**
 * This plugin is meant to be used in conjunction with the ScrollMagic animation framework.
 * It offers an easy API to __trigger__ ScrollMagic animations.
 *
 * With the current version of ScrollMagic scrollbound animations (scenes with duration) are not supported.
 * This feature will be added as soon as ScrollMagic provides the appropriate API.
 *
 * To have access to this extension, please include `plugins/animation.scrollmagic.js`.
 * @requires {@link http://julian.com/research/scrollmagic/|ScrollMagic ~1.2.0}
 * @mixin animation.ScrollMagic
 */
(function (root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['ScrollMagic', 'scrollmagic'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('scrollmagic'), require('scrollmagic'));
	} else {
		// Browser globals
		factory(root.ScrollMagic || (root.jQuery && root.jQuery.ScrollMagic), root.ScrollMagic || (root.jQuery && root.jQuery.ScrollMagic));
	}
}(this, function (ScrollMagic, scrollmagic) {
	"use strict";
	var NAMESPACE = "animation.scrollmagic";

	var
		console = window.console || {},
		err = Function.prototype.bind.call(console.error || console.log || function () {}, console);
	if (!ScrollMagic) {
		err("(" + NAMESPACE + ") -> ERROR: The ScrollMagic main module could not be found. Please make sure it's loaded before this plugin or use an asynchronous loader like requirejs.");
	}
	if (!scrollmagic) {
		err("(" + NAMESPACE + ") -> ERROR: ScrollMagic could not be found. Please make sure it's loaded before ScrollMagic or use an asynchronous loader like requirejs.");
	}

	var autoindex = 0;

	ScrollMagic.Scene.extend(function () {
		var
			Scene = this,
			_util = ScrollMagic._util,
			_currentProgress = 0,
			_elems,
			_properties,
			_options,
			_dataID; // used to identify element data related to this scene, will be defined everytime a new scrollmagic animation is added

		var log = function () {
			if (Scene._log) { // not available, when main source minified
				Array.prototype.splice.call(arguments, 1, 0, "(" + NAMESPACE + ")", "->");
				Scene._log.apply(this, arguments);
			}
		};

		// set listeners
		Scene.on("progress.plugin_scrollmagic", function () {
			updateAnimationProgress();
		});
		Scene.on("destroy.plugin_scrollmagic", function (e) {
			Scene.off("*.plugin_scrollmagic");
			Scene.removeScrollMagic(e.reset);
		});

		var animate = function (elem, properties, options) {
			if (_util.type.Array(elem)) {
				elem.forEach(function (elem) {
					animate(elem, properties, options);
				});
			} else {
				// set reverse values
				if (!scrollmagic.Utilities.data(elem, _dataID)) {
					scrollmagic.Utilities.data(elem, _dataID, {
						reverseProps: _util.css(elem, Object.keys(_properties))
					});
				}
				// animate
				scrollmagic(elem, properties, options);
				if (options.queue !== undefined) {
					scrollmagic.Utilities.dequeue(elem, options.queue);
				}
			}
		};
		var reverse = function (elem, options) {
			if (_util.type.Array(elem)) {
				elem.forEach(function (elem) {
					reverse(elem, options);
				});
			} else {
				var data = scrollmagic.Utilities.data(elem, _dataID);
				if (data && data.reverseProps) {
					scrollmagic(elem, data.reverseProps, options);
					if (options.queue !== undefined) {
						scrollmagic.Utilities.dequeue(elem, options.queue);
					}
				}
			}
		};

		/**
		 * Update the tween progress to current position.
		 * @private
		 */
		var updateAnimationProgress = function () {
			if (_elems) {
				var progress = Scene.progress();
				if (progress != _currentProgress) { // do we even need to update the progress?
					if (Scene.duration() === 0) {
						// play the animation
						if (progress > 0) { // play forward
							animate(_elems, _properties, _options);
						} else { // play reverse
							reverse(_elems, _options);
							// scrollmagic(_elems, _propertiesReverse, _options);
							// scrollmagic("reverse");
						}
					} else {
						// TODO: Scrollbound animations not supported yet...
					}
					_currentProgress = progress;
				}
			}
		};

		/**
		 * Add a ScrollMagic animation to the scene.
		 * The method accepts the same parameters as ScrollMagic, with the first parameter being the target element.
		 *
		 * To gain better understanding, check out the [ScrollMagic example](../examples/basic/simple_scrollmagic.html).
		 * @memberof! animation.ScrollMagic#
		 *
		 * @example
		 * // trigger a ScrollMagic animation
		 * scene.setScrollMagic("#myElement", {opacity: 0.5}, {duration: 1000, easing: "linear"});
		 *
		 * @param {(object|string)} elems - One or more Dom Elements or a Selector that should be used as the target of the animation.
		 * @param {object} properties - The CSS properties that should be animated.
		 * @param {object} options - Options for the animation, like duration or easing.
		 * @returns {Scene} Parent object for chaining.
		 */
		Scene.setScrollMagic = function (elems, properties, options) {
			if (_elems) { // kill old ani?
				Scene.removeScrollMagic();
			}

			_elems = _util.get.elements(elems);
			_properties = properties || {};
			_options = options || {};
			_dataID = "ScrollMagic." + NAMESPACE + "[" + (autoindex++) + "]";

			if (_options.queue !== undefined) {
				// we'll use the queue to identify the animation. When defined it will always stop the previously running one.
				// if undefined the animation will always fully run, as is expected.
				// defining anything other than 'false' as the que doesn't make much sense, because ScrollMagic takes control over the trigger.
				// thus it is also overwritten.
				_options.queue = _dataID + "_queue";
			}

			var checkDuration = function () {
				if (Scene.duration() !== 0) {
					log(1, "ERROR: The ScrollMagic animation plugin does not support scrollbound animations (scenes with duration) yet.");
				}
			};
			Scene.on("change.plugin_scrollmagic", function (e) {
				if (e.what == 'duration') {
					checkDuration();
				}
			});
			checkDuration();
			log(3, "added animation");

			updateAnimationProgress();
			return Scene;
		};
		/**
		 * Remove the animation from the scene.
		 * This will stop the scene from triggering the animation.
		 *
		 * Using the reset option you can decide if the animation should remain in the current state or be rewound to set the target elements back to the state they were in before the animation was added to the scene.
		 * @memberof! animation.ScrollMagic#
		 *
		 * @example
		 * // remove the animation from the scene without resetting it
		 * scene.removeScrollMagic();
		 *
		 * // remove the animation from the scene and reset the elements to initial state
		 * scene.removeScrollMagic(true);
		 *
		 * @param {boolean} [reset=false] - If `true` the animation will rewound.
		 * @returns {Scene} Parent object for chaining.
		 */
		Scene.removeScrollMagic = function (reset) {
			if (_elems) {
				// stop running animations
				if (_options.queue !== undefined) {
					scrollmagic(_elems, "stop", _options.queue);
				}
				if (reset) {
					reverse(_elems, {
						duration: 0
					});
				}

				_elems.forEach(function (elem) {
					scrollmagic.Utilities.removeData(elem, _dataID);
				});
				_elems = _properties = _options = _dataID = undefined;
			}
			return Scene;
		};
	});
}));
