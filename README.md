INTRODUCTION
------------

This module integrates the 'ScrollMagic.js' library
  - https://github.com/janpaepke/ScrollMagic

'ScrollMagic.js' The javascript library for magical scroll interactions.

ScrollMagic helps you to easily react to the user's current scroll position.
It's the perfect library for you, if you want to ...

  - Animate based on scroll position – either trigger an animation or
    synchronize it to the scrollbar movement (like a playback scrub control).

  - Pin an element starting at a specific scroll position, either indefinitely
    or for a limited amount of scroll progress (sticky elements).

  - Toggle CSS classes of elements on and off based on scroll position.

  - Effortlessly add parallax effects to your website.

  - Create an infinitely scrolling page (ajax load of additional content).

  - Add callbacks at specific scroll positions or while scrolling past
    a specific section, passing a progress parameter.


FEATURES
--------

  - Optimized performance

  - Lightweight (6KB gzipped)

  - Flexibility and extensibility

  - Mobile compatibility

  - Event management

  - Support for responsive web design

  - Object-oriented programming and object chaining

  - Readable, centralized code, and intuitive development

  - Support for both x and y direction scrolling (even both on one page)

  - Support for scrolling inside div containers (even multiple on one page)

  - Extensive debugging and logging capabilities

  - Detailed documentation

  - Many application examples


REQUIREMENTS
------------

No requirement (library already included).


INSTALLATION
------------

Quick and easy to use that load ScrollMagic library and UI pack on your pages:

1. Download 'ScrollMagic' module - https://www.drupal.org/project/scrollmagic

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/scrollmagic or /modules/scrollmagic

3. Now, enable 'ScrollMagic' module.

If you want to have more control over how to load, change the version,
use or not use the UI pack on pages (to limit it on some pages),
you can also activate the "ScrollMagic UI" submodule too.


USAGE
-----

It’s very simple to use a library, Add your script in theme/module js file.

How does ScrollMagic work?
The principle design pattern of ScrollMagic is a controller that has
an arbitrary number of scenes attached to it.

There is one controller for each scroll container. In most cases there is only
one controller object and the scroll container is the browser window. But you
can also use DIV elements for scrolling and even have multiple containers on
your page. The controller also defines which direction should be scrolled
(horizontal or vertical) and is responsible for keeping all scenes updated.

A scene defines what should happen when, meaning at which scroll position.
It can trigger animations, pin an element, change element classes or anything
else you might desire.


Defining the Controller
=======================
As mentioned above in most cases the scroll container is the browser window.
To create a ScrollMagic controller with the default settings we use the main
ScrollMagic.Controller() class. We create a new instance of it and assign it
to a variable, so we can reference it later:

var controller = new ScrollMagic.Controller();

That's it! Now for the more interesting part:


Defining Scenes
===============
Scenes are created by using the ScrollMagic.Scene() class. A ScrollMagic.Scene
defines where the controller should react and how. Here we define a variable
called “scene” and we'll create a new ScrollMagic.Scene() instance.

var scene = new ScrollMagic.Scene();
Inside ScrollMagic.Scene we can place an object of associated properties and
values that are made available according to the docs…
These options describe the behavior of our Scene and in order to figure out
what value has what effect you can play around in the Scene Manipulation.

var scene = new ScrollMagic.Scene({
  offset: 100, // start scene after scrolling for 100px.
  duration: 400 // pin the element for 400px of scrolling.
})


Adding Scenes to Controller
===========================
In order to have the scenes react to the scrolling of the container
we have to add our scene to the controller we defined at the very beginning…

var scene = new ScrollMagic.Scene({
  // starting scene, when reaching this element.
  triggerElement: '#pinned-trigger1',
  duration: 400 // pin the element for a total of 400px.
})
.setPin('#pinned-element1'); // the element we want to pin

// Add Scene to ScrollMagic Controller.
controller.addScene(scene);
If you desire multiple scenes at once you can pass them to the controller
just as the example shows below:

// Add Scene to ScrollMagic Controller.
controller.addScene([
  scene1,
  scene2,
  scene3
]);
Instead of telling the controller what scenes to add you can also tell
the scene to be added to a certain controller:

var scene = new ScrollMagic.Scene({
  triggerElement: '#trigger1'
})
.addTo(controller); // Add Scene to ScrollMagic Controller.

var scene2 = new ScrollMagic.Scene({
  triggerElement: '#trigger2'
})
.addTo(controller); // Add Scene to ScrollMagic Controller.
In the above we're using a technique called "chaining" with the addTo().
If no Semicolon ends the line, we can continue adding ScrollMagic.Scene
methods and "chain" them together.

For more usage check official document:

  - https://scrollmagic.io/docs/index.html


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://scrollmagic.io/
